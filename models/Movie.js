const debug = require('debug')('server:Movie');
const mongoose = require('mongoose');

const { Schema } = mongoose;

const MovieSchema = new Schema(
  {
  title: { type: String, required: true },
  year: { type: Number, required: true },
  image: String,
  }, {
    toJSON: {
      transform: (doc, ret) => {
        ret.id = doc._id;
        delete ret._id;
        delete ret.__v;
        return ret;
      }
    },
  })

const Movie = mongoose.model('Movie', MovieSchema);

module.exports = Movie;