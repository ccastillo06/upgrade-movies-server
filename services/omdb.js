const fetch = require('node-fetch');
const qs = require('qs');

class Omdb {
  constructor() {
    this.apiKey = process.env.OMDB_API_KEY;
    this.baseUrl = process.env.OMDB_BASE_URL;
  }

  buildUrl(params) {
    const queryParams = qs.stringify(params);
    return `${this.baseUrl}?apiKey=${this.apiKey}&${queryParams}`;
  }

  /**
   * Search a movie by a given title
   * @param {string} title 
   * @return {Promise}
   */
  search(title, type) {
    const url = this.buildUrl({ s: title, type });
    return fetch(url);
  }

  get(id, type) {
    const url = this.buildUrl({ i: id, type });
    return fetch(url);
  }
}

const omdb = new Omdb();

module.exports = omdb;