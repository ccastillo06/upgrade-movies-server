const debug = require('debug')('server:movies');

const omdb = require('../services/omdb');
const Movie = require('../models/Movie');

const searchMovie = async (req, res) => {
  const { title, type } = req.query;

  try {
    const response = await omdb.search(title, type);
    const data = await response.json();

    return res.status(200).json(data);
  } catch (err) {
    return res.status(400).json(err.message);
  }
};

const getById = async (req, res) => {
  const { id, type } = req.query;

  try {
    const movie = await Movie.findOne(id);

    if (movie) {
      return res.status(200).json(movie);
    }
  } catch (e) {
    try {
      const response = await omdb.get(id, type);
      const data = await response.json();

      return res.status(200).json(data);
    } catch (err) {
      return res.status(400).json(err.message);
    }
  }
};

const getAll = async (req, res) => {
  try {
    const movies = await Movie.find();

    return res.status(200).json(movies);
  } catch (err) {
    return res.status(500).json(err.message);
  }
};

const createMovie = async (req, res) => {
  const { title, year, image } = req.body;

  try {
    const newMovie = new Movie({
      title,
      year,
      image,
    });

    const movie = await newMovie.save();
    return res.status(200).json(movie);
  } catch (err) {
    return res.status(500).json(err.message);
  }
};

module.exports = {
  getById,
  searchMovie,
  getAll,
  createMovie,
};
